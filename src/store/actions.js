import { REGISTER_USER } from "./types";

export const registerUser = (data) => {
  return { type: REGISTER_USER, payload: data };
};
