export const emailErrorMessage =
  "That doesn't look to be valid email address. Please review and try again.";
export const passwordMatchErrorMessage =
  "Your passwords do not match. Please review and try again.";
