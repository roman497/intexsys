import axios from "axios";

const api = (url, data) => {
  axios.post(url, data).then(
    (response) => {
      console.log(response);
    },
    (error) => {
      console.log(error);
    }
  );
};

export const register = (data) =>
  api("http://localhost:3000/registerApi", data);

export const signIn = (data) => api("http://localhost:3000/signInApi", data);

export const forgot = (data) => api("http://localhost:3000/forgotApi", data);
