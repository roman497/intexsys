import React, { useState } from "react";
import { signIn } from "../../services/api";
import { validateEmail } from "../../services/utils";
import { emailErrorMessage } from "../../services/constants";
import { Link } from "react-router-dom";
import { Header, Form, Input, Button, Error } from "../../atoms";
import "./SignIn.scss";

const SignIn = () => {
  const [emailError, setEmailError] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);

  const validate = (e) => {
    e.preventDefault();

    setEmailError(validateEmail(email));

    if (email && !emailError && emailError !== null && password) {
      signIn({ email, password });
    }
  };

  return (
    <div className="signin">
      <Header title="Sign in" />
      <Form onSubmit={(e) => validate(e)}>
        <Input
          type="text"
          label="Email"
          id="email"
          onChange={(e) => setEmail(e.target.value)}
        />
        {emailError && <Error message={emailErrorMessage} />}
        <Input
          type="password"
          label="Password"
          id="password"
          isEnterPasword={!!password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <div className="signin-options">
          <div>
            <input type="checkbox" id="keep" />
            <label htmlFor="keep">Keep me signed in</label>
          </div>
          <div className="signin-forgot">
            <Link to="/forgot">Forgot Your Password?</Link>
          </div>
        </div>
        <Button type="submit" isGreen value="Sign In" />
        <Button link="/register" value="Register Now" />
      </Form>
    </div>
  );
};

export default SignIn;
