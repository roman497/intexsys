import React, { useState } from "react";
import { validateEmail } from "../../services/utils";
import { emailErrorMessage } from "../../services/constants";
import { Link } from "react-router-dom";
import { forgot } from "../../services/api";
import { Header, Form, Input, Button, Error } from "../../atoms";
import "./Forgot.scss";

const Forgot = () => {
  const [emailError, setEmailError] = useState(null);
  const [email, setEmail] = useState(null);

  const validate = (e) => {
    e.preventDefault();

    setEmailError(validateEmail(email));

    if (email && !emailError && emailError !== null) {
      forgot({ email });
    }
  };

  return (
    <div className="forgot">
      <Header
        title="Forgot Your Password?"
        description="Please enter your enail address below and allow for a few minutes to receive the password"
      />
      <Form onSubmit={(e) => validate(e)}>
        <Input
          type="text"
          label="Email"
          id="email"
          onChange={(e) => setEmail(e.target.value)}
        />
        {emailError && <Error message={emailErrorMessage} />}
        <Button type="submit" isGreen value="SEND" />
        <div className="forgot-options">
          <Link to="/signin">Sign In</Link>or
          <Link to="/register">Register</Link>
        </div>
      </Form>
    </div>
  );
};

export default Forgot;
