import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { validateEmail } from "../../services/utils";
import {
  emailErrorMessage,
  passwordMatchErrorMessage,
} from "../../services/constants";
import { registerUser } from "../../store/actions";
import { register } from "../../services/api";
import { Header, Form, Input, Button, Error } from "../../atoms";

const Register = () => {
  const [emailError, setEmailError] = useState(null);
  const [passwordMatchError, setPasswordMatchError] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [confirmPassword, setConfirmPassword] = useState(null);

  let history = useHistory();
  const dispatch = useDispatch();

  const validate = (e) => {
    e.preventDefault();
    setEmailError(validateEmail(email));
    setPasswordMatchError(
      password !== confirmPassword ||
        (password === null && confirmPassword === null)
    );

    if (
      email &&
      !emailError &&
      emailError !== null &&
      !passwordMatchError &&
      passwordMatchError !== null
    ) {
      register({ email, password }); // Just call api. Could be implemented in different ways from actions as well.
      dispatch(registerUser({ email, password }));
      history.push("/signin");
    }
  };

  return (
    <>
      <Header title="Register Your Account" />
      <Form onSubmit={(e) => validate(e)}>
        <Input
          type="text"
          label="Email"
          isStar
          onChange={(e) => setEmail(e.target.value)}
        />
        {emailError && <Error message={emailErrorMessage} />}
        <Input
          type="password"
          label="Password"
          isEnterPasword={!!password}
          isStar
          onChange={(e) => setPassword(e.target.value)}
        />
        <Input
          type="password"
          label="Re-type Password"
          isEnterPasword={!!confirmPassword}
          isStar
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        {passwordMatchError && <Error message={passwordMatchErrorMessage} />}
        <Button type="submit" isGreen value="Register Your Account" />
        <Button link="/signin" value="Sign In" />
      </Form>
    </>
  );
};

export default Register;
