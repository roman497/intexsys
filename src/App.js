import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { SignIn, Register, Forgot } from "./components";
import "./App.css";

const App = () => (
  <div className="app">
    <Router>
      <Switch>
        <Route exact path="/">
          <SignIn />
        </Route>
        <Route path="/signin">
          <SignIn />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Route path="/forgot">
          <Forgot />
        </Route>
      </Switch>
    </Router>
  </div>
);

export default App;
