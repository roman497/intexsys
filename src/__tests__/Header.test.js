import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Header from "../atoms/Header";

describe("Header", () => {
  const title = "title";
  const description = "description";

  let container = null;

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it("should be title", () => {
    act(() => {
      render(<Header title={title} />, container);
    });
    const headerTitle = document.querySelector(".header-title");
    expect(headerTitle.innerHTML).toEqual(title);
  });

  it("should be description", () => {
    act(() => {
      render(<Header description={description} />, container);
    });
    const headerDescription = document.querySelector(".header-description");
    expect(headerDescription.innerHTML).toEqual(description);
  });
});
