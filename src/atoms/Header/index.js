import React from "react";
import logo from "../../assets/images/logo.png";
import "./Header.scss";

const Header = ({ title, description }) => (
  <div className="header">
    <div className="header-logo">
      <img src={logo} alt="Optics planet" />
    </div>
    <div className="header-title">{title}</div>
    <div className="header-description">{description}</div>
  </div>
);

export default Header;
