import React, { useState } from "react";
import "./Input.scss";

const Input = ({
  type,
  label,
  id,
  onChange,
  isEnterPasword = false,
  isStar = false,
}) => {
  const [isToggleOn, setIsToggleOn] = useState(true);

  const handleClick = (e) => {
    e.preventDefault();
    setIsToggleOn(!isToggleOn);
  };

  return (
    <div className="input">
      <label
        htmlFor={id}
        className={isStar ? "input-label star" : "input-label"}
      >
        {label}
      </label>
      <input
        type={isEnterPasword && isToggleOn ? "password" : "text" || type}
        id={id}
        autoComplete="off"
        onChange={onChange}
      />
      {isEnterPasword ? (
        <div className="input-showPassword">
          <a href="/#" onClick={handleClick}>
            {isToggleOn ? "Show" : "Hide"}
          </a>
        </div>
      ) : null}
    </div>
  );
};

export default Input;
