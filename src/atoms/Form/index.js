import React from "react";

const Form = ({ children, onSubmit }) => (
  <div>
    <form onSubmit={onSubmit}>{children}</form>
  </div>
);

export default Form;
