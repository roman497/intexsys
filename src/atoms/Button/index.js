import React from "react";
import { Link } from "react-router-dom";
import "./Button.scss";

const Button = ({ type, value, isGreen, link = null }) => (
  <div className="button">
    {link ? (
      <div className="link"><Link to={link}>{value}</Link></div>
    ) : (
      <input
        type={type}
        className={isGreen ? "green" : "white"}
        value={value}
      />
    )}
  </div>
);

export default Button;
